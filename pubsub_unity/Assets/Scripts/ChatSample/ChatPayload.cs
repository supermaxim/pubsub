﻿public class ChatPayload
{
    public string UserId { get; set; }

    public string Text { get; set; }
}
